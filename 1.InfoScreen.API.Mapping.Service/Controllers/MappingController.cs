﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _2.InfoScreen.API.Mapping.Application.Commands.MappingCommands;
using _2.InfoScreen.API.Mapping.Application.Requests.CRUD;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Result;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace _1.InfoScreen.API.Mapping.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MappingController : ControllerBase
    {
        private IMediator _mediator;
        public MappingController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [Route("CreateManyMappings")]
        public async Task<ActionResult<Result<List<MappingDTO>>>> CreateManyMappings([FromBody] List<MappingDTO> dtos)
        {
            Result<List<MappingDTO>> response = 
                await _mediator.Send(
                    new CreateManyCommand(
                        new CreateManyRequest<MappingDTO>() {DTOs = dtos }));

            return new ObjectResult(response);
        }

        [HttpGet]
        [Route("GetManyMappingsByEndpointAndTemplateId/{endpointId}/{templateId}")]
        public async Task<ActionResult<Result<List<MappingDTO>>>> GetManyMappingsByEndpointAndTemplateId([FromRoute] int endpointId, [FromRoute] int templateId)
        {
            Result<List<MappingDTO>> response =
                await _mediator.Send(
                    new GetManyByEndpointAndTemplateIdCommand(
                        new GetManyByEndpointAndTemplateIdRequest() {EndpointId = endpointId, TemplateId = templateId }));

            return new ObjectResult(response);
        }

        [HttpDelete]
        [Route("DeleteManyMappingsByEndpointAndTemplateId/{endpointId}/{templateId}")]
        public async Task<ActionResult<Result<List<MappingDTO>>>> DeleteManyMappingsByEndpointAndTemplateId([FromRoute] int endpointId, [FromRoute] int templateId)
        {
            Result<List<MappingDTO>> response =
                await _mediator.Send(
                    new DeleteManyByEndpointAndTemplateIdCommand(
                        new DeleteManyByEndpointAndTemplateIdRequest() { EndpointId = endpointId, TemplateId = templateId }));

            return new ObjectResult(response);
        }

        [HttpPut]
        [Route("UpdateManyMappings")]
        public async Task<ActionResult<Result<List<MappingDTO>>>> UpdateManyMappings([FromBody] List<MappingDTO> dtos)
        {
            Result<List<MappingDTO>> response =
                await _mediator.Send(
                    new UpdateManyCommand(
                        new UpdateManyRequest<MappingDTO>() { DTOs = dtos }));

            return new ObjectResult(response);
        }

    }
}