﻿using _2.InfoScreen.API.Mapping.Application.Requests.CRUD;
using _2.InfoScreen.API.Mapping.Application.Requests.MappedData;
using _2.InfoScreen.API.Mapping.Application.Validators.Interface;
using _2.InfoScreen.API.Mapping.Application.Validators.MappingValidators;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Mapping.Application.Validators.Manager
{
    public class MappingValidateManager : IMappingValidateManager
    {
        private readonly IValidator<string> _externalFieldValidator;
        private readonly IValidator<string> _dynamicFieldValidator;
        private readonly IValidator<int> _idValidator;
        private readonly IValidator<int> _viewEndpointIdValidator;
        private readonly IValidator<int> _endpointIdValidator;

        public MappingValidateManager()
        {
            _dynamicFieldValidator = new DynamicFieldValidator();
            _externalFieldValidator = new ExternalFieldValidator();
            _idValidator = new IdValidator();
            _viewEndpointIdValidator = new ViewEndpointIdValidator();
            _endpointIdValidator = new EndpointIdValidator();
        }

        public void ValidataGetMappedDataRequest(GetMappedDataRequest request)
        {
            _viewEndpointIdValidator.ValidateAndThrow(request.TemplateId);
            _endpointIdValidator.ValidateAndThrow(request.EndpointId);

        }

        public void ValidateCreateManyRequest(CreateManyRequest<MappingDTO> request)
        {
            foreach(var item in request.DTOs)
            {
                _viewEndpointIdValidator.ValidateAndThrow(item.EndpointId);
                _viewEndpointIdValidator.ValidateAndThrow(item.TemplateId);
                _dynamicFieldValidator.ValidateAndThrow(item.DynamicFieldName);
                _externalFieldValidator.ValidateAndThrow(item.ExternalFieldName);
            }
        }

        public void ValidateDeleteManyByViewEndpointIdRequest(DeleteManyByEndpointAndTemplateIdRequest request)
        {
            _idValidator.ValidateAndThrow(request.EndpointId);
            _idValidator.ValidateAndThrow(request.TemplateId);
        }

        public void ValidateGetManyByViewEndpointIdRequest(GetManyByEndpointAndTemplateIdRequest request)
        {
            _idValidator.ValidateAndThrow(request.EndpointId);
            _idValidator.ValidateAndThrow(request.TemplateId);
        }

        public void ValidateUpdateManyRequest(UpdateManyRequest<MappingDTO> request)
        {
            foreach (var item in request.DTOs)
            {
                _idValidator.ValidateAndThrow(item.Id);
                _viewEndpointIdValidator.ValidateAndThrow(item.EndpointId);
                _viewEndpointIdValidator.ValidateAndThrow(item.TemplateId);
                _dynamicFieldValidator.ValidateAndThrow(item.DynamicFieldName);
                _externalFieldValidator.ValidateAndThrow(item.ExternalFieldName);
            }
        }
    }
}
