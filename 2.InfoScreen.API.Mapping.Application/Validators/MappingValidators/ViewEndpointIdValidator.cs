﻿using _4.InfoScreen.API.Mapping.Domain.ValidationMessages;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Mapping.Application.Validators.MappingValidators
{
    public class ViewEndpointIdValidator : AbstractValidator<int>
    {
        public ViewEndpointIdValidator()
        {
            RuleFor(x => x).
                NotNull().
                WithMessage(ErrorMessages.ViewEndpointIdNullMessage).
                GreaterThan(0).
                WithMessage(ErrorMessages.ViewEndpointIdInvalidMessage);
        }
    }
}
