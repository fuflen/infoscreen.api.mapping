﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Mapping.Application.Requests.CRUD
{
    public class DeleteManyByEndpointAndTemplateIdRequest
    {
        public int EndpointId { get; set; }
        public int TemplateId { get; set; }
    }
}
