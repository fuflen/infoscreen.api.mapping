﻿using _2.InfoScreen.API.Mapping.Application.Commands.MappingCommands;
using _2.InfoScreen.API.Mapping.Application.Validators.Interface;
using _3.InfoScreen.API.Mapping.Infrastructure.Interfaces;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Result;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using _3.InfoScreen.API.Mapping.Infrastructure.Service;

namespace _2.InfoScreen.API.Mapping.Application.Handlers.MappingHandlers
{
    public class GetManyByEndpointAndTemplateIdHandler : IRequestHandler<GetManyByEndpointAndTemplateIdCommand, Result<List<MappingDTO>>>
    {
        private IMapper _mapper;
        private IMappingRepository _repo;
        private IMappingValidateManager _validateManager;
        private RabbitMQConnectionService __rabbitmq;
        public GetManyByEndpointAndTemplateIdHandler(IMapper mapper, IMappingRepository repo, IMappingValidateManager validateManager, RabbitMQConnectionService rabbitmq)
        {
            __rabbitmq = rabbitmq;
            _mapper = mapper;
            _repo = repo;
            _validateManager = validateManager;
        }

        public async Task<Result<List<MappingDTO>>> Handle(GetManyByEndpointAndTemplateIdCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateGetManyByViewEndpointIdRequest(request.RequestModel);

                var models = await _repo.GetManyByEndpointAndTemplateID(request.RequestModel.EndpointId, request.RequestModel.TemplateId);

                List<MappingDTO> dtos = _mapper.Map<List<MappingDTO>>(models);
                return Result<List<MappingDTO>>.Success(dtos);
            }
            catch(Exception e)
            {
                return Result<List<MappingDTO>>.Error(e);
            }

        }
    }
}
