﻿using _4.InfoScreen.API.Mapping.Domain.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.Mapping.Infrastructure.Interfaces
{
    public interface IMappingRepository : IGenericRepository<MappingModel>
    {
        Task<List<MappingModel>> CreateMany(List<MappingModel> entities);
        Task<List<MappingModel>> GetManyByEndpointAndTemplateID(int endpointId, int templateId);
        Task<List<MappingModel>> DeleteManyByEndpointAndTemplateID(int endpointId, int templateId);
        Task<List<MappingModel>> UpdateMany(List<MappingModel> entities);
        Task<List<MappingModel>> DeleteManyByEndpointId(int endpointId);
        Task<List<MappingModel>> DeleteManyByTemplateId(int templateId);
        Task<List<MappingModel>> UndoDeleteManyByTemplateAndEndpointId(int templateId, int endpointId);
        Task<List<MappingModel>> UndoDeleteManyByTemplateId(int templateId);
        Task<List<MappingModel>> UndoDeleteManyByEndpointId(int endpointId);
    }
}

