﻿using _2.InfoScreen.API.Mapping.Application.Requests.CRUD;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Mapping.Application.Commands.MappingCommands
{
    public class CreateManyCommand: IRequest<Result<List<MappingDTO>>>
    {
        public CreateManyCommand(CreateManyRequest<MappingDTO> requestModel)
        {
            RequestModel = requestModel;
        }

        public CreateManyRequest<MappingDTO> RequestModel { get; set; }
    }
}
