﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Mapping.Domain.Models.RabbitMQModels.MessageDTO.OutgoingMessages
{
    public class MappingDeletedTemplateMessage
    {
        public int TemplateId { get; set; }
    }
}
