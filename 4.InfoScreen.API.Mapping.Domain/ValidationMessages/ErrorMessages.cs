﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Mapping.Domain.ValidationMessages
{
    public class ErrorMessages
    {
        //ExternalFieldValidator
        public const string ExternalFieldNullMessage = "ExternalField value is required cannot be null";
        public const string ExternalFieldEmptyMessage = "ExternalField value is required cannot be empty";

        //DynamicFieldValidatr
        public const string DynamicFieldNullMessage = "DynamicField value is required cannot be null";
        public const string DynamicFieldEmptyMessage = "DynamicField value is required cannot be empty";

        //Id
        public const string IdNullMessage = "Id value is required cannot be null";
        public const string IdInvalidMessage = "Id value must be greater than zero";

        //ViewEndpointId
        public const string ViewEndpointIdNullMessage = "ViewEndpointId value is required cannot be null";
        public const string ViewEndpointIdInvalidMessage = "ViewEndpointId value must be greater than zero";

        //ViewEndpointId
        public const string EndpointIdNullMessage = "EndpointId value is required cannot be null";
        public const string EndpointIdInvalidMessage = "EndpointId value must be greater than zero";
    }
}
