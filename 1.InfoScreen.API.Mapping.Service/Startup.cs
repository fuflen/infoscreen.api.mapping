﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using _2.InfoScreen.API.Mapping.Application;
using _2.InfoScreen.API.Mapping.Application.Managers;
using _2.InfoScreen.API.Mapping.Application.Validators.Interface;
using _2.InfoScreen.API.Mapping.Application.Validators.Manager;
using _3.InfoScreen.API.Mapping.Infrastructure.Context;
using _3.InfoScreen.API.Mapping.Infrastructure.Interfaces;
using _3.InfoScreen.API.Mapping.Infrastructure.Repositories;
using _3.InfoScreen.API.Mapping.Infrastructure.Service.MappedData;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using _2.InfoScreen.API.Mapping.Application.CustomHealthChecks;
using _3.InfoScreen.API.Mapping.Infrastructure.HostedService;
using _3.InfoScreen.API.Mapping.Infrastructure.Service;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using HealthChecks.UI.Client;

namespace _1.InfoScreen.API.Mapping.Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            //Registers the DbContext in the ServiceCollection and Creates a In-memory Database
            //with the given name.
            services.AddDbContext<MappingContext>(options =>
                options.UseSqlServer(Configuration["Connectionstring"])
            );
            services.AddAutoMapper();
            services.AddScoped<IMappingRepository, MappingRepository>();

            services.AddHealthChecks()
                .AddCheck<MemoryHealthCheck>("Memory_check")
                .AddUrlGroup(uri =>
                    {
                        uri.AddUri(new Uri("http://integrationservice/swagger"));
                        uri.ExpectHttpCode((int)HttpStatusCode.OK);
                        uri.UseGet();
                    }, "endpointServiceConnection_check", HealthStatus.Unhealthy)
                .AddRabbitMQ(@"amqp://guest:guest@rabbitmq:5672", null, "rabbitMQConnection_check", HealthStatus.Unhealthy)
                .AddSqlServer(Configuration["Connectionstring"], "SELECT 1;", "SqlServerConnection_check", HealthStatus.Unhealthy);

            services.AddMediatR(typeof(Assembly));

            services.AddTransient<IDbInitializer, DbInitializer>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "Mapping API",
                    Description = "An API for mapping dynamic fields and getting mapped data",
                    Version = "v1"
                });
            });

            services.AddTransient<RabbitMQConnectionService>();
            services.AddTransient<RabbitMQMappingService>();

            services.AddTransient<IMappingValidateManager, MappingValidateManager>();
            services.AddHttpClient<IIntegrationService, IntegrationService>();
            services.AddTransient<MappedDataManager>();

            services.AddHostedService<RabbitMQHostedService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            {
                // Initialize the database
                var services = scope.ServiceProvider;
                var dbContext = services.GetService<MappingContext>();
                var dbInitializer = services.GetService<IDbInitializer>();
                dbInitializer.Initialize(dbContext);
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHealthChecks("/health", new HealthCheckOptions
            {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse,
                Predicate = registration => true
            });

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Mapping API v1");
            });

            //app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
