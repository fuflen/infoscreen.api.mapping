﻿using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Models.Entities;
using _4.InfoScreen.API.Mapping.Domain.Result;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace _2.InfoScreen.API.Mapping.Application.Managers
{
    public class MappedDataManager
    {
        public Result<List<MappedDynamicModelDTO>> GetMappedData(List<MappingModel> mappings, List<DynamicModelDTO> dynamicModels)
        {
            try
            {
                if (mappings == null || !mappings.Any())
                    throw new ArgumentNullException($"No data found in {nameof(mappings)}");

                if (dynamicModels == null || !dynamicModels.Any())
                    throw new ArgumentNullException($"No data found in {nameof(dynamicModels)}");

                List<MappedDynamicModelDTO> mappedModelDtos = new List<MappedDynamicModelDTO>();

                foreach (var model in dynamicModels)
                {
                    MappedDynamicModelDTO mappedModelDTO = new MappedDynamicModelDTO();

                    JObject mappedModelJObject = new JObject();

                    var dynamicModelJObject = JObject.Parse(model.DTOModel.ToString());

                    foreach (var map in mappings)
                    {
                        var nests = map.ExternalFieldName.ToString().Split('.').ToList();

                        var obj = GetNestedObject(dynamicModelJObject, nests);
                        var fieldValue = obj.ToString();

                        mappedModelJObject.Add(map.DynamicFieldName, fieldValue);
                    }
                    mappedModelDTO.DTOModel = mappedModelJObject.ToObject(typeof(object));

                    mappedModelDtos.Add(mappedModelDTO);
                }
                return Result<List<MappedDynamicModelDTO>>.Success(mappedModelDtos);
            }
            catch(Exception ex)
            {
                return Result<List<MappedDynamicModelDTO>>.Error(ex, ex.Message);
            }
        }
        
        private JToken GetNestedObject(JToken obj, List<string> fields)
        {
            if (fields.Count() > 1)
            {
                return GetNestedObject(obj[fields[0]], fields.Skip(1).ToList());
            }
            else
            {
                return obj[fields[0]];
            }
        }
        
    }
}
