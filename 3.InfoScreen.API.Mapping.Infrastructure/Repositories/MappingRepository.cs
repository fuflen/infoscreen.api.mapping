﻿using _3.InfoScreen.API.Mapping.Infrastructure.Context;
using _3.InfoScreen.API.Mapping.Infrastructure.Interfaces;
using _4.InfoScreen.API.Mapping.Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.Mapping.Infrastructure.Repositories
{
    public class MappingRepository : GenericRepository<MappingModel>, IMappingRepository
    {
        private MappingContext _dbContext;
        public MappingRepository(MappingContext dbContext)
        : base(dbContext)
        {
            dbContext.Database.EnsureCreated();
            _dbContext = dbContext;
        }

        public async Task<List<MappingModel>> CreateMany(List<MappingModel> entities)
        {
            await _dbContext.Set<MappingModel>().AddRangeAsync(entities);
            await _dbContext.SaveChangesAsync();
            return entities;
        }

        public async Task<List<MappingModel>> DeleteManyByEndpointAndTemplateID(int endpointId, int templateId)
        {
            var entitiesToRemove = await GetManyByEndpointAndTemplateID(endpointId, templateId);
            entitiesToRemove.ForEach(x => x.Deleted = true);
            _dbContext.Set<MappingModel>().UpdateRange(entitiesToRemove);
            await _dbContext.SaveChangesAsync();
            return entitiesToRemove;
        }

        public async Task<List<MappingModel>> GetManyByEndpointAndTemplateID(int endpointId, int templateId)
        {
            return await _dbContext.Set<MappingModel>()
                .Where(x => x.EndpointId == endpointId && x.TemplateId == templateId && !x.Deleted).ToListAsync();

        }

        public async Task<List<MappingModel>> UpdateMany(List<MappingModel> entities)
        {
            entities.ForEach(x => x.Deleted = false);
            _dbContext.Set<MappingModel>().UpdateRange(entities);
            await _dbContext.SaveChangesAsync();
            return entities;
        }

        private async Task<List<MappingModel>> GetManyByEndpointId(int endpointId)
        {
            return await _dbContext.Set<MappingModel>()
                .Where(x => x.EndpointId == endpointId).ToListAsync();

        }

        public async Task<List<MappingModel>> DeleteManyByEndpointId(int endpointId)
        {
            var entitiesToRemove = await GetManyByEndpointId(endpointId);
            entitiesToRemove.ForEach(x => x.Deleted = true);
            _dbContext.Set<MappingModel>().UpdateRange(entitiesToRemove);
            await _dbContext.SaveChangesAsync();
            return entitiesToRemove;
        }

        private async Task<List<MappingModel>> GetManyByTemplateId(int templateId)
        {
            return await _dbContext.Set<MappingModel>().Where(x => x.TemplateId == templateId).ToListAsync();
        }

        public async Task<List<MappingModel>> DeleteManyByTemplateId(int templateId)
        {
            var entitiesToRemove = await GetManyByTemplateId(templateId);
            entitiesToRemove.ForEach(x => x.Deleted = true);
            _dbContext.Set<MappingModel>().UpdateRange(entitiesToRemove);
            await _dbContext.SaveChangesAsync();
            return entitiesToRemove;
        }

        public async Task<List<MappingModel>> UndoDeleteManyByEndpointId(int endpointId)
        {
            var entitiesToRemove = await GetManyByEndpointId(endpointId);
            entitiesToRemove.ForEach(x => x.Deleted = false);
            _dbContext.Set<MappingModel>().UpdateRange(entitiesToRemove);
            await _dbContext.SaveChangesAsync();
            return entitiesToRemove;
        }

        public async Task<List<MappingModel>> UndoDeleteManyByTemplateId(int templateId)
        {
            var entitiesToRemove = await GetManyByTemplateId(templateId);
            entitiesToRemove.ForEach(x => x.Deleted = false);
            _dbContext.Set<MappingModel>().UpdateRange(entitiesToRemove);
            await _dbContext.SaveChangesAsync();
            return entitiesToRemove;
        }

        public async Task<List<MappingModel>> UndoDeleteManyByTemplateAndEndpointId(int templateId, int endpointId)
        {
            var entitiesToRemove = await GetManyByEndpointAndTemplateID(endpointId, templateId);
            entitiesToRemove.ForEach(x => x.Deleted = false);
            _dbContext.Set<MappingModel>().UpdateRange(entitiesToRemove);
            await _dbContext.SaveChangesAsync();
            return entitiesToRemove;
        }


    }
}
