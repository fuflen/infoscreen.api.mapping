﻿using _4.InfoScreen.API.Mapping.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Mapping.Application.Requests.CRUD
{
    public class UpdateManyRequest<T> where T : DTOInterface
    {
        public List<T> DTOs { get; set; }
    }
}
