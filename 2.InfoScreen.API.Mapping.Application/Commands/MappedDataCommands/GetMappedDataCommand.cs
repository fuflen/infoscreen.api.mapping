﻿using _2.InfoScreen.API.Mapping.Application.Requests.MappedData;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Mapping.Application.Commands.MappedDataCommands
{
    public class GetMappedDataCommand : IRequest<Result<List<MappedDynamicModelDTO>>>
    {
        public GetMappedDataCommand(GetMappedDataRequest requestModel)
        {
            RequestModel = requestModel;
        }

        public GetMappedDataRequest RequestModel { get; set; }
    }
}
