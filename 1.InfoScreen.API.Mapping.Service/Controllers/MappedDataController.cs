﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _2.InfoScreen.API.Mapping.Application.Commands.MappedDataCommands;
using _2.InfoScreen.API.Mapping.Application.Requests.MappedData;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Result;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace _1.InfoScreen.API.Mapping.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MappedDataController : ControllerBase
    {
        public IMediator _mediator;
        public MappedDataController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("GetMappedData/{endpointId}/{templateId}")]
        public async Task<ActionResult<Result<List<MappedDynamicModelDTO>>>> GetMappedData([FromRoute] int endpointId, [FromRoute] int templateId)
        {
            Result<List<MappedDynamicModelDTO>> response =
                await _mediator.Send(
                    new GetMappedDataCommand(
                        new GetMappedDataRequest() { EndpointId = endpointId, TemplateId = templateId }));

            return new ObjectResult(response);
        }
    }
}