﻿using _2.InfoScreen.API.Mapping.Application.Managers;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace _5.InfoScreen.API.Mapping.UnitTests.MappedDataManagerTests
{
    public class GetMappedDataTests
    {
        [Theory]
        [MemberData(nameof(GetMappedDataTestDataGenerator.GetValidData),
            MemberType = typeof(GetMappedDataTestDataGenerator))]
        public void DynamicModelResolver_ValidMemberData_ConversionPassed(List<MappingModel> mappings, List<DynamicModelDTO> dynamicModels, List<MappedDynamicModelDTO> expectedResult)
        {
            MappedDataManager manager = new MappedDataManager();

            var result = manager.GetMappedData(mappings, dynamicModels).Data;

            //Assert
            Assert.Equal(expectedResult.ToString(), result.ToString());
        }

        [Theory]
        [MemberData(nameof(GetMappedDataTestDataGenerator.GetInvalidData),MemberType = typeof(GetMappedDataTestDataGenerator))]
        public void DynamicModelResolver_InvalidMemberData_ConversionFailed(List<MappingModel> mappings, List<DynamicModelDTO> dynamicModels, Exception expectedException)
        {
            MappedDataManager manager = new MappedDataManager();

            var result = manager.GetMappedData(mappings, dynamicModels);

            //Assert
            var x = result.Exception.GetType().IsEquivalentTo(expectedException.GetType());
            Assert.True(x);
        }
    }
}
