﻿using _3.InfoScreen.API.Mapping.Infrastructure.Context;
using _3.InfoScreen.API.Mapping.Infrastructure.Interfaces;
using _4.InfoScreen.API.Mapping.Domain.Models.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.Mapping.Infrastructure.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity>
    where TEntity : class, IEntity
    {
        private readonly MappingContext _dbContext;

        /// <summary>
        /// The constructor for the Generic Repository
        /// </summary>
        /// <param name="dbContext">The context to use. </param>
        public GenericRepository(MappingContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Saved the given Entity to its corresponding DbSet table. 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<TEntity> Create(TEntity entity)
        {
            await _dbContext.Set<TEntity>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }

        /// <summary>
        /// Deleted the Entity with the given id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TEntity> Delete(int id)
        {
            var entity = await GetById(id);
            _dbContext.Set<TEntity>().Remove(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }

        /// <summary>
        /// Get the whole DbSet for the Entity.
        /// AsNoTracking as it improved performance. And Eliminated Reference loops. 
        /// </summary>
        /// <returns>IQuerable of <see cref="TEntity"/> so they can be further queued </returns>
        public IQueryable<TEntity> GetAll()
        {
            return  _dbContext.Set<TEntity>().AsNoTracking();
        }

        /// <summary>
        /// Gets the Entity with the given id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TEntity> GetById(int id)
        {
            return await _dbContext.Set<TEntity>()
                            .AsNoTracking()
                            .FirstOrDefaultAsync(e => e.Id == id);
        }

        /// <summary>
        /// Updates the entity wíth the given id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity">The updated Entity</param>
        /// <returns></returns>
        public async Task<TEntity> Update(int id, TEntity entity)
        {
            var oldEntity = await GetById(id);
            entity.Deleted = oldEntity.Deleted;

            var updatedEntity = _dbContext.Set<TEntity>().Update(entity).Entity;
            await _dbContext.SaveChangesAsync();

            return updatedEntity;
        }
    }
}
