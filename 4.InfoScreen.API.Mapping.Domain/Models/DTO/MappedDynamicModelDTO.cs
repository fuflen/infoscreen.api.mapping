﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Mapping.Domain.Models.DTO
{
    public class MappedDynamicModelDTO
    {
        public dynamic DTOModel { get; set; }
    }
}
