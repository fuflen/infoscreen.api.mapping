﻿using _2.InfoScreen.API.Mapping.Application.Requests.CRUD;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Mapping.Application.Commands.MappingCommands
{
    public class GetManyByEndpointAndTemplateIdCommand : IRequest<Result<List<MappingDTO>>>
    {
        public GetManyByEndpointAndTemplateIdCommand(GetManyByEndpointAndTemplateIdRequest requestModel)
        {
            RequestModel = requestModel;
        }

        public GetManyByEndpointAndTemplateIdRequest RequestModel {get;set;}
    }
}
