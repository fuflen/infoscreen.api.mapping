﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using _4.InfoScreen.API.Mapping.Domain.Models.Entities;

namespace _3.InfoScreen.API.Mapping.Infrastructure.Context
{
    public class DbInitializer : IDbInitializer
    {
        public void Initialize(MappingContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            if (context.MappingModels.Any())
            {
                return;
            }

            List<MappingModel> dataEndpoints = new List<MappingModel>
            {
                new MappingModel { DynamicFieldName = "field1", ExternalFieldName = "id", EndpointId = 3, TemplateId = 1},
                new MappingModel { DynamicFieldName = "field2", ExternalFieldName = "employee_age", EndpointId = 3, TemplateId = 1},
                new MappingModel { DynamicFieldName = "field3", ExternalFieldName = "employee_name", EndpointId = 3, TemplateId = 1},
                new MappingModel { DynamicFieldName = "field4", ExternalFieldName = "employee_salary", EndpointId = 3, TemplateId = 1},

                new MappingModel { DynamicFieldName = "field1", ExternalFieldName = "name", EndpointId = 1,TemplateId = 1},
                new MappingModel { DynamicFieldName = "field2", ExternalFieldName = "gender", EndpointId = 1, TemplateId = 1},
                new MappingModel { DynamicFieldName = "field3", ExternalFieldName = "hair_color", EndpointId = 1, TemplateId = 1},
                new MappingModel { DynamicFieldName = "field4", ExternalFieldName = "eye_color", EndpointId = 1, TemplateId = 1},

                new MappingModel { DynamicFieldName = "field1", ExternalFieldName = "name", EndpointId = 2, TemplateId = 1},
                new MappingModel { DynamicFieldName = "field2", ExternalFieldName = "climate", EndpointId = 2, TemplateId = 1},
                new MappingModel { DynamicFieldName = "field3", ExternalFieldName = "terrain", EndpointId = 2, TemplateId = 1},
                new MappingModel { DynamicFieldName = "field4", ExternalFieldName = "edited", EndpointId = 2, TemplateId = 1},

                new MappingModel { DynamicFieldName = "field1", ExternalFieldName = "name", EndpointId = 5, TemplateId = 1},
                new MappingModel { DynamicFieldName = "field2", ExternalFieldName = "classification", EndpointId = 5, TemplateId = 1},
                new MappingModel { DynamicFieldName = "field3", ExternalFieldName = "skin_colors", EndpointId = 5, TemplateId = 1},
                new MappingModel { DynamicFieldName = "field4", ExternalFieldName = "language", EndpointId = 5, TemplateId = 1}
            };

            context.MappingModels.AddRange(dataEndpoints);
            context.SaveChanges();
        }
    }
}
