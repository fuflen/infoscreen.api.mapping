﻿using _2.InfoScreen.API.Mapping.Application.Requests.CRUD;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Result;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Mapping.Application.Commands.MappingCommands
{
    public class DeleteManyByEndpointAndTemplateIdCommand: IRequest<Result<List<MappingDTO>>>
    {
        public DeleteManyByEndpointAndTemplateIdCommand(DeleteManyByEndpointAndTemplateIdRequest requestModel)
        {
            RequestModel = requestModel;
        }
        public DeleteManyByEndpointAndTemplateIdRequest RequestModel { get; set; }
    }
}
