﻿using _2.InfoScreen.API.Mapping.Application.Requests.CRUD;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Result;
using MediatR;
using System.Collections.Generic;

namespace _2.InfoScreen.API.Mapping.Application.Commands.MappingCommands
{
    public class UpdateManyCommand : IRequest<Result<List<MappingDTO>>>
    {
        public UpdateManyCommand(UpdateManyRequest<MappingDTO> requestModel)
        {
            RequestModel = requestModel;
        }

        public UpdateManyRequest<MappingDTO> RequestModel { get; set; }
    }
}
