﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3.InfoScreen.API.Mapping.Infrastructure.Interfaces
{
    public interface IMessagingGateway
    {
        void SendMappingDeletedEndpointMessage(int endpointId);

        void SendMappingDeletedTemplateMessage(int templateId);

        void SendMappingDeletedViewMessage(int endpointId, int templateId, int viewId);

    }
}
