﻿using _4.InfoScreen.API.Mapping.Domain.Models;
using _4.InfoScreen.API.Mapping.Domain.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace _3.InfoScreen.API.Mapping.Infrastructure.Context
{
    public class MappingContext : DbContext
    {
        public MappingContext(DbContextOptions<MappingContext> options)
            : base(options)
        {
        }

        public DbSet<MappingModel> MappingModels { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            
            builder.Entity<MappingModel>(entity =>
            {
                entity.HasKey(x => x.Id);
                entity.Property(x => x.Id).ForSqlServerUseSequenceHiLo();
                entity.Property(x => x.DynamicFieldName).IsRequired();
                entity.Property(x => x.ExternalFieldName).IsRequired();
                entity.Property(x => x.EndpointId).IsRequired();
                entity.Property(x => x.TemplateId).IsRequired();
            });

            base.OnModelCreating(builder);
        }
    }
}
