﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Mapping.Domain.Models.RabbitMQModels.MessageDTO
{
    public class RabbitMQConnectionModel
    {
        public IModel Model { get; set; }
        public IBasicProperties Properties { get; set; }
        public string ExchangeName { get; set; }
    }
}
