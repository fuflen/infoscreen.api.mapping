﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using _3.InfoScreen.API.Mapping.Infrastructure.Interfaces;
using _4.InfoScreen.API.Mapping.Domain.Models.RabbitMQModels.MessageDTO;

namespace _3.InfoScreen.API.Mapping.Infrastructure.Service
{
    public class RabbitMQConnectionService
    {
        private static RabbitMQConnectionModel _connectionModel;

        private const string ExchangeName = "EventExchange";

        public RabbitMQConnectionService()
        {
            IConnection connection;
            var factory = new ConnectionFactory
            {
                HostName = "rabbitmq",
                Port = 5672,
                UserName = "guest",
                Password = "guest"
            };
            while (true)
            {
                Console.WriteLine("Connection starting!!!");
                try
                {
                    connection = factory.CreateConnection();
                    Console.WriteLine("Connection started!!!");
                    break;
                }
                catch (Exception e)
                {
                    Thread.Sleep(5000);
                    Console.WriteLine(e);
                }
            }

            var model = connection.CreateModel();

            var properties = model.CreateBasicProperties();
            properties.Persistent = true;

            model.ExchangeDeclare(ExchangeName, "topic");

            _connectionModel = new RabbitMQConnectionModel
            {
                Properties = properties,
                ExchangeName = ExchangeName,
                Model = model
            };
        }

        public RabbitMQConnectionModel GetConnectionModel()
        {
            return _connectionModel;
        }
    }
}
