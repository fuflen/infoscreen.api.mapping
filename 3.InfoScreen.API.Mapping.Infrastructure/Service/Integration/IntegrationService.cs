﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Result;
using Newtonsoft.Json;

namespace _3.InfoScreen.API.Mapping.Infrastructure.Service.MappedData
{
    public class IntegrationService : IIntegrationService
    {
        private HttpClient _client;
        public IntegrationService(HttpClient client)
        {
            _client = client;
        }

        public async Task<Result<List<DynamicModelDTO>>> GetDataFromIntegrationServiceAsync(int endpointId)
        {

            HttpResponseMessage response = await _client.GetAsync("http://integrationservice/api/integration/GetDataFromEndpointId/" + endpointId);

            var dto = await response.Content.ReadAsAsync<Result<List<DynamicModelDTO>>>();

            return dto;
        }
    }
}
