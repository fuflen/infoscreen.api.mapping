﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3.InfoScreen.API.Mapping.Infrastructure.Context
{
    public interface IDbInitializer
    {
        void Initialize(MappingContext context);
    }
}
