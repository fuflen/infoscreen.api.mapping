﻿using _4.InfoScreen.API.Mapping.Domain.ValidationMessages;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Mapping.Application.Validators.MappingValidators
{
    public class EndpointIdValidator : AbstractValidator<int>
    {
        public EndpointIdValidator()
        {
            RuleFor(x => x).
                NotNull().
                WithMessage(ErrorMessages.EndpointIdNullMessage).
                GreaterThan(0).
                WithMessage(ErrorMessages.EndpointIdInvalidMessage);
        }
    }
}
