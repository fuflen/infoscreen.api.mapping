﻿using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Result;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace _3.InfoScreen.API.Mapping.Infrastructure.Service.MappedData
{
    public interface IIntegrationService
    {
        Task<Result<List<DynamicModelDTO>>> GetDataFromIntegrationServiceAsync(int endpointId);
    }
}
