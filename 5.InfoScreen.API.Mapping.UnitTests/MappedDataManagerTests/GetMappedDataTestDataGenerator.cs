﻿using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Models.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace _5.InfoScreen.API.Mapping.UnitTests.MappedDataManagerTests
{
    public class GetMappedDataTestDataGenerator
    {
        static List<MappingModel> mappedModels = new List<MappingModel>()
        {
            new MappingModel(){DynamicFieldName = "field1", EndpointId = 1, ExternalFieldName = "name", Id = 5, TemplateId = 1},
            new MappingModel(){DynamicFieldName = "field2", EndpointId = 1, ExternalFieldName = "gender", Id = 6, TemplateId = 1},
            new MappingModel(){DynamicFieldName = "field3", EndpointId = 1, ExternalFieldName = "hair_color", Id = 7, TemplateId = 1},
            new MappingModel(){DynamicFieldName = "field4", EndpointId = 1, ExternalFieldName = "eye_color", Id = 8, TemplateId = 1},
        };
        static List<DynamicModelDTO> dynamicModelDTOs = new List<DynamicModelDTO>()
        {
            new DynamicModelDTO()
            {
                DTOModel = "{\r\n  \"name\": \"Luke Skywalker\",\r\n  \"height\": \"172\",\r\n  \"mass\": \"77\",\r\n  \"hair_color\": \"blond\",\r\n  \"skin_color\": \"fair\",\r\n  \"eye_color\": \"blue\",\r\n  \"birth_year\": \"19BBY\",\r\n  \"gender\": \"male\",\r\n  \"homeworld\": \"https://swapi.co/api/planets/1/\",\r\n  \"films\": [\r\n    \"https://swapi.co/api/films/2/\",\r\n    \"https://swapi.co/api/films/6/\",\r\n    \"https://swapi.co/api/films/3/\",\r\n    \"https://swapi.co/api/films/1/\",\r\n    \"https://swapi.co/api/films/7/\"\r\n  ],\r\n  \"species\": [\r\n    \"https://swapi.co/api/species/1/\"\r\n  ],\r\n  \"vehicles\": [\r\n    \"https://swapi.co/api/vehicles/14/\",\r\n    \"https://swapi.co/api/vehicles/30/\"\r\n  ],\r\n  \"starships\": [\r\n    \"https://swapi.co/api/starships/12/\",\r\n    \"https://swapi.co/api/starships/22/\"\r\n  ],\r\n  \"created\": \"2014-12-09T13:50:51.644Z\",\r\n  \"edited\": \"2014-12-20T21:17:56.891Z\",\r\n  \"url\": \"https://swapi.co/api/people/1/\"\r\n}"
            },
            new DynamicModelDTO()
            {
                DTOModel = "{\r\n  \"name\": \"C-3PO\",\r\n  \"height\": \"167\",\r\n  \"mass\": \"75\",\r\n  \"hair_color\": \"n/a\",\r\n  \"skin_color\": \"gold\",\r\n  \"eye_color\": \"yellow\",\r\n  \"birth_year\": \"112BBY\",\r\n  \"gender\": \"n/a\",\r\n  \"homeworld\": \"https://swapi.co/api/planets/1/\",\r\n  \"films\": [\r\n    \"https://swapi.co/api/films/2/\",\r\n    \"https://swapi.co/api/films/5/\",\r\n    \"https://swapi.co/api/films/4/\",\r\n    \"https://swapi.co/api/films/6/\",\r\n    \"https://swapi.co/api/films/3/\",\r\n    \"https://swapi.co/api/films/1/\"\r\n  ],\r\n  \"species\": [\r\n    \"https://swapi.co/api/species/2/\"\r\n  ],\r\n  \"vehicles\": [],\r\n  \"starships\": [],\r\n  \"created\": \"2014-12-10T15:10:51.357Z\",\r\n  \"edited\": \"2014-12-20T21:17:50.309Z\",\r\n  \"url\": \"https://swapi.co/api/people/2/\"\r\n}"
            },
            new DynamicModelDTO()
            {
                DTOModel = "{\r\n  \"name\": \"R2-D2\",\r\n  \"height\": \"96\",\r\n  \"mass\": \"32\",\r\n  \"hair_color\": \"n/a\",\r\n  \"skin_color\": \"white, blue\",\r\n  \"eye_color\": \"red\",\r\n  \"birth_year\": \"33BBY\",\r\n  \"gender\": \"n/a\",\r\n  \"homeworld\": \"https://swapi.co/api/planets/8/\",\r\n  \"films\": [\r\n    \"https://swapi.co/api/films/2/\",\r\n    \"https://swapi.co/api/films/5/\",\r\n    \"https://swapi.co/api/films/4/\",\r\n    \"https://swapi.co/api/films/6/\",\r\n    \"https://swapi.co/api/films/3/\",\r\n    \"https://swapi.co/api/films/1/\",\r\n    \"https://swapi.co/api/films/7/\"\r\n  ],\r\n  \"species\": [\r\n    \"https://swapi.co/api/species/2/\"\r\n  ],\r\n  \"vehicles\": [],\r\n  \"starships\": [],\r\n  \"created\": \"2014-12-10T15:11:50.376Z\",\r\n  \"edited\": \"2014-12-20T21:17:50.311Z\",\r\n  \"url\": \"https://swapi.co/api/people/3/\"\r\n}"
            }
        };
        static List<DynamicModelDTO> EmptyDynamicModelDTOs = new List<DynamicModelDTO>();

        static List<MappingModel> EmptyMappedModels = new List<MappingModel>();


        static List<MappedDynamicModelDTO> expectedMappedDynamicModels;

        public static IEnumerable<object[]> GetValidData()
        {
            expectedMappedDynamicModels = new List<MappedDynamicModelDTO>() {
            new MappedDynamicModelDTO(){DTOModel = JObject.Parse(
                    "{\r\n  \"field1\": \"Luke Skywalker\",\r\n" +
                    "\"field2\": \"male\",\r\n" +
                    "\"field3\": \"blond\",\r\n" +
                    "\"field4\": \"blue\"\r\n}").ToObject(typeof(object))
                },
                new MappedDynamicModelDTO(){DTOModel = JObject.Parse(
                    "{\r\n  \"field1\": \"C-3PO\",\r\n" +
                    "\"field2\": \"n/a\",\r\n" +
                    "\"field3\": \"n/a\",\r\n" +
                    "\"field4\": \"yellow\"\r\n}").ToObject(typeof(object))
                },
                new MappedDynamicModelDTO(){DTOModel = JObject.Parse(
                    "{\r\n  \"field1\": \"R2-D2\",\r\n" +
                    "\"field2\": \"n/a\",\r\n" +
                    "\"field3\": \"n/a\",\r\n" +
                    "\"field4\": \"red\"\r\n}").ToObject(typeof(object))
                },
        };

            yield return new object[] { mappedModels, dynamicModelDTOs, expectedMappedDynamicModels };
        }

        public static IEnumerable<object[]> GetInvalidData()
        {
            yield return new object[] { mappedModels, EmptyDynamicModelDTOs, new ArgumentNullException() };
            yield return new object[] { EmptyMappedModels, EmptyDynamicModelDTOs, new ArgumentNullException() };
            yield return new object[] { mappedModels, null, new ArgumentNullException() };
            yield return new object[] { null, EmptyDynamicModelDTOs, new ArgumentNullException() };
        }


    }
}
