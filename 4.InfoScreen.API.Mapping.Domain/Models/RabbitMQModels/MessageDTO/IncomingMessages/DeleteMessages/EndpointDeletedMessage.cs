﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Mapping.Domain.Models.MessageDTO
{
    public class EndpointDeletedMessage
    {
        public int EndpointId { get; set; }
    }
}
