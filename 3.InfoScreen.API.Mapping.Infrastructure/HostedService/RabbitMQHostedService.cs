﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Framing.Impl;
using _3.InfoScreen.API.Mapping.Infrastructure.Interfaces;
using _3.InfoScreen.API.Mapping.Infrastructure.Service;
using _4.InfoScreen.API.Mapping.Domain.Models.MessageDTO;
using _4.InfoScreen.API.Mapping.Domain.Models.RabbitMQModels.MessageDTO;
using _4.InfoScreen.API.Mapping.Domain.Models.RabbitMQModels.MessageDTO.DeleteFailedMessages;

namespace _3.InfoScreen.API.Mapping.Infrastructure.HostedService
{
    public class RabbitMQHostedService : IHostedService
    {
        private RabbitMQConnectionModel _connectionModel;
        private IServiceProvider _services;

        private const string DeleteQueue = "delete_mappings";

        private const string EndpointDeletedBindingKey = "endpoint.deleted";
        private const string ViewDeletedBindingKey = "view.deleted";
        private const string TemplateDeletedBindingKey = "delete.template";

        private const string UndoDeleteQueue = "undo_delete_mappings";

        private const string ViewDeleteFailedEndpointBindingKey = "view.delete.failed.endpoint";
        private const string ViewDeleteFailedTemplateBindingKey = "view.delete.failed.template";
        private const string ScheduleDeleteFailedViewBindingKey = "schedule.delete.failed.view";


        public RabbitMQHostedService(RabbitMQConnectionService connection, IServiceProvider services)
        {
            _services = services;
            _connectionModel = connection.GetConnectionModel();
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            ConfigureDeleteQueue();

            ConfigureUndoDeleteQueue();
        }

        private void ConfigureDeleteQueue()
        {
            _connectionModel.Model.QueueDeclare(DeleteQueue, true, false, false, null);

            _connectionModel.Model.QueueBind(DeleteQueue, _connectionModel.ExchangeName, EndpointDeletedBindingKey);
            _connectionModel.Model.QueueBind(DeleteQueue, _connectionModel.ExchangeName, ViewDeletedBindingKey);
            _connectionModel.Model.QueueBind(DeleteQueue, _connectionModel.ExchangeName, TemplateDeletedBindingKey);

            Console.WriteLine("queue binded!!!");

            var consumer = new EventingBasicConsumer(_connectionModel.Model);

            consumer.Received += async (model, ea) =>
            {
                await HandleReceivedDeleteEvent(ea);
            };

            Console.WriteLine("CONSOLE METHOD WRITELINE");

            _connectionModel.Model.BasicConsume(queue: DeleteQueue,
                autoAck: true,
                consumer: consumer);
        }

        private void ConfigureUndoDeleteQueue()
        {
            _connectionModel.Model.QueueDeclare(UndoDeleteQueue, true, false, false, null);

            _connectionModel.Model.QueueBind(UndoDeleteQueue, _connectionModel.ExchangeName, ViewDeleteFailedEndpointBindingKey);
            _connectionModel.Model.QueueBind(UndoDeleteQueue, _connectionModel.ExchangeName, ViewDeleteFailedTemplateBindingKey);
            _connectionModel.Model.QueueBind(UndoDeleteQueue, _connectionModel.ExchangeName, ScheduleDeleteFailedViewBindingKey);

            Console.WriteLine("queue binded!!!");

            var consumer = new EventingBasicConsumer(_connectionModel.Model);

            consumer.Received += async (model, ea) =>
            {
                await HandleReceivedUndoDeleteEvent(ea);
            };

            Console.WriteLine("CONSOLE METHOD WRITELINE");

            _connectionModel.Model.BasicConsume(queue: UndoDeleteQueue,
                autoAck: true,
                consumer: consumer);
        }

        private async Task HandleReceivedDeleteEvent(BasicDeliverEventArgs ea)
        {
            Console.WriteLine("Handler started");

            switch (ea.RoutingKey)
            {
                case EndpointDeletedBindingKey:
                    EndpointDeletedMessage endpoingMessage = Deserialize<EndpointDeletedMessage>(ea.Body);
                    await DeleteMappingsWithEndpoint(endpoingMessage.EndpointId);
                    break;
                case ViewDeletedBindingKey:
                    Console.WriteLine("Converting JSON");
                    ViewDeletedMessage viewMessage = Deserialize<ViewDeletedMessage>(ea.Body);
                    await DeleteMappingsWithView(viewMessage.EndpointId, viewMessage.TemplateId, viewMessage.ViewId);
                    break;
                case TemplateDeletedBindingKey:
                    Console.WriteLine("Converting JSON");
                    TemplateDeletedMessage templateMessage = Deserialize<TemplateDeletedMessage>(ea.Body);
                    await DeleteMappingsWithTemplate(templateMessage.TemplateId);
                    break;
            }
        }

        private async Task HandleReceivedUndoDeleteEvent(BasicDeliverEventArgs ea)
        {
            Console.WriteLine("Handler started");

            switch (ea.RoutingKey)
            {
                case ViewDeleteFailedEndpointBindingKey:
                    ViewDeleteFailedEndpointMessage endpoingMessage = Deserialize<ViewDeleteFailedEndpointMessage>(ea.Body);
                    await UndoDeleteMappingsWithEndpoint(endpoingMessage.EndpointId);
                    break;
                case ViewDeleteFailedTemplateBindingKey:
                    Console.WriteLine("Converting JSON");
                    ViewDeleteFailedTemplateMessage viewMessage = Deserialize<ViewDeleteFailedTemplateMessage>(ea.Body);
                    await UndoDeleteMappingsWithTemplate(viewMessage.TemplateId);
                    break;
                case ScheduleDeleteFailedViewBindingKey:
                    Console.WriteLine("Converting JSON");
                    ScheduleDeleteFailedViewMessage scheduleMessage = Deserialize<ScheduleDeleteFailedViewMessage>(ea.Body);
                    await UndoDeleteMappingWithView(scheduleMessage.EndpointId, scheduleMessage.TemplateId, scheduleMessage.ViewId);
                    break;
            }
        }

        public static T Deserialize<T>(byte[] data) where T : class
        {
            using (var stream = new MemoryStream(data))
            using (var reader = new StreamReader(stream, Encoding.UTF8))
                return JsonSerializer.Create().Deserialize(reader, typeof(T)) as T;
        }

        private async Task DeleteMappingsWithEndpoint(int endpointId)
        {
            Console.WriteLine("Deleting Mappings With Endpoint: " + endpointId);
            using (var scope = _services.CreateScope())
            {
                var mappingRepo =
                    scope.ServiceProvider
                        .GetRequiredService<IMappingRepository>();

                var messageService =
                    scope.ServiceProvider
                        .GetRequiredService<RabbitMQMappingService>();

                try
                {
                    await mappingRepo.DeleteManyByEndpointId(endpointId);

                    messageService.SendMappingDeletedEndpointMessage(endpointId);

                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                    messageService.SendMappingDeleteFailedEndpointMessage(endpointId);
                }
            }
        }

        

        private async Task DeleteMappingsWithTemplate(int templateId)
        {
            Console.WriteLine("Deleting Mappings With Template: " + templateId);
            using (var scope = _services.CreateScope())
            {
                var mappingRepo =
                    scope.ServiceProvider
                        .GetRequiredService<IMappingRepository>();

                var messageService =
                    scope.ServiceProvider
                        .GetRequiredService<RabbitMQMappingService>();

                try
                {
                    await mappingRepo.DeleteManyByTemplateId(templateId);

                    messageService.SendMappingDeletedTemplateMessage(templateId);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                    messageService.SendMappingDeleteFailedTemplateMessage(templateId);
                }
            }
        }

        private async Task DeleteMappingsWithView(int endpointId, int templateId, int viewId)
        {
            Console.WriteLine("Deleting Mappings in general.. Endpoint: " + endpointId + " Template: " + templateId);
            using (var scope = _services.CreateScope())
            {
                var mappingRepo =
                    scope.ServiceProvider
                        .GetRequiredService<IMappingRepository>();

                var messageService =
                    scope.ServiceProvider
                        .GetRequiredService<RabbitMQMappingService>();

                try
                {

                    await mappingRepo.DeleteManyByEndpointAndTemplateID(endpointId, templateId);

                    messageService.SendMappingDeletedViewMessage(endpointId, templateId, viewId);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e.Message);
                    messageService.SendMappingDeleteFailedViewMessage(endpointId, templateId, viewId);
                }
            }
        }

        private async Task UndoDeleteMappingsWithEndpoint(int endpointId)
        {
            Console.WriteLine("Deleting Mappings With Endpoint: " + endpointId);
            try
            {
                using (var scope = _services.CreateScope())
                {
                    var mappingRepo =
                        scope.ServiceProvider
                            .GetRequiredService<IMappingRepository>();

                    var messageService =
                    scope.ServiceProvider
                        .GetRequiredService<RabbitMQMappingService>();


                    await mappingRepo.UndoDeleteManyByEndpointId(endpointId);
                    messageService.SendMappingDeleteFailedEndpointMessage(endpointId);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }
        

        private async Task UndoDeleteMappingsWithTemplate(int templateId)
        {
            Console.WriteLine("Deleting Mappings With Endpoint: " + templateId);
            try
            {
                using (var scope = _services.CreateScope())
                {
                    var mappingRepo =
                        scope.ServiceProvider
                            .GetRequiredService<IMappingRepository>();

                    var messageService =
                    scope.ServiceProvider
                        .GetRequiredService<RabbitMQMappingService>();


                    await mappingRepo.UndoDeleteManyByTemplateId(templateId);
                    messageService.SendMappingDeleteFailedTemplateMessage(templateId);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        private async Task UndoDeleteMappingWithView(int endpointId, int templateId, int viewId)
        {
            Console.WriteLine("Deleting Mappings With Endpoint: " + endpointId);
            try
            {
                using (var scope = _services.CreateScope())
                {
                    var mappingRepo =
                        scope.ServiceProvider
                            .GetRequiredService<IMappingRepository>();

                    var messageService =
                    scope.ServiceProvider
                        .GetRequiredService<RabbitMQMappingService>();


                    await mappingRepo.UndoDeleteManyByTemplateAndEndpointId(endpointId, templateId);
                    messageService.SendMappingDeleteFailedViewMessage(endpointId, templateId, viewId);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                _connectionModel.Model.Dispose();
                cancellationToken.ThrowIfCancellationRequested();
            });
        }
    }
}
