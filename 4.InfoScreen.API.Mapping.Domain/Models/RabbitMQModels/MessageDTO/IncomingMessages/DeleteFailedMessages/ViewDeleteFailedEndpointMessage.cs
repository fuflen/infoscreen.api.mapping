﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Mapping.Domain.Models.RabbitMQModels.MessageDTO.DeleteFailedMessages
{
    public class ViewDeleteFailedEndpointMessage
    {
        public int EndpointId { get; set; }
    }
}
