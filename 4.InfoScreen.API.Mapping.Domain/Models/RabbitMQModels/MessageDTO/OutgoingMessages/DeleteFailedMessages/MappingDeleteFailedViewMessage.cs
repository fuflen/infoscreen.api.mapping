﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Mapping.Domain.Models.RabbitMQModels.MessageDTO.OutgoingMessages.DeleteFailedMessages
{
    public class MappingDeleteFailedViewMessage
    {
        public int ViewId { get; set; }
        public int EndpointId { get; set; }
        public int TemplateId { get; set; }
    }
}
