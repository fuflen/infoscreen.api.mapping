﻿using _4.InfoScreen.API.Mapping.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Mapping.Domain.Models.Entities
{
    public class MappingModel : IEntity
    {
        public int Id { get; set; }
        public string DynamicFieldName { get; set; }
        public string ExternalFieldName { get; set; }
        public int EndpointId { get; set; }
        public int TemplateId { get; set; }
        public bool Deleted { get; set; }
    }
}
