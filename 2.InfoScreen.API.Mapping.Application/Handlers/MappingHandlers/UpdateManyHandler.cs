﻿using _2.InfoScreen.API.Mapping.Application.Commands.MappingCommands;
using _2.InfoScreen.API.Mapping.Application.Validators.Interface;
using _3.InfoScreen.API.Mapping.Infrastructure.Interfaces;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Models.Entities;
using _4.InfoScreen.API.Mapping.Domain.Result;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _2.InfoScreen.API.Mapping.Application.Handlers.MappingHandlers
{
    public class UpdateManyHandler : IRequestHandler<UpdateManyCommand, Result<List<MappingDTO>>>
    {
        private IMapper _mapper;
        private IMappingRepository _repo;
        private IMappingValidateManager _validateManager;
        public UpdateManyHandler(IMapper mapper, IMappingRepository repo, IMappingValidateManager validateManager)
        {
            _repo = repo;
            _mapper = mapper;
            _validateManager = validateManager;
        }
        public async Task<Result<List<MappingDTO>>> Handle(UpdateManyCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateUpdateManyRequest(request.RequestModel);
                var entitiesToUpdate = _mapper.Map<List<MappingModel>>(request.RequestModel.DTOs);

                var updatedEntites = await _repo.UpdateMany(entitiesToUpdate);

                var dtos = _mapper.Map<List<MappingDTO>>(updatedEntites);

                return Result<List<MappingDTO>>.Success(dtos);
            }
            catch(Exception e)
            {
                return  Result<List<MappingDTO>>.Error(e);
            }
        }
    }
}
