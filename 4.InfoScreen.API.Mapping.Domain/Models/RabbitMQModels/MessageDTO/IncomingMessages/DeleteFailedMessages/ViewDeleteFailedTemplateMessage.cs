﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Mapping.Domain.Models.RabbitMQModels.MessageDTO.DeleteFailedMessages
{
    public class ViewDeleteFailedTemplateMessage
    {
        public int TemplateId { get; set; }
    }
}
