﻿using _4.InfoScreen.API.Mapping.Domain.ValidationMessages;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Mapping.Application.Validators.MappingValidators
{
    class IdValidator : AbstractValidator<int>
    {
        public IdValidator()
        {
            RuleFor(x => x).
                NotNull().
                WithMessage(ErrorMessages.IdNullMessage).
                GreaterThan(0).
                WithMessage(ErrorMessages.IdInvalidMessage);
        }
    }
}
