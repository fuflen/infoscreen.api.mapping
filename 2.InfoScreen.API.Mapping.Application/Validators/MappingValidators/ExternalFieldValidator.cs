﻿using _4.InfoScreen.API.Mapping.Domain.ValidationMessages;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Mapping.Application.Validators.MappingValidators
{
    class ExternalFieldValidator : AbstractValidator<string>
    {
        public ExternalFieldValidator()
        {
            RuleFor(x => x).
                NotNull().
                WithMessage(ErrorMessages.ExternalFieldNullMessage).
                NotEmpty().
                WithMessage(ErrorMessages.ExternalFieldEmptyMessage);
        }
    }
}
