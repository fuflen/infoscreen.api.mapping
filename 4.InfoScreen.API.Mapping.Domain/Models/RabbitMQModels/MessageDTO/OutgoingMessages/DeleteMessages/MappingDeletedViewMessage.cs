﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4.InfoScreen.API.Mapping.Domain.Models.RabbitMQModels.MessageDTO.OutgoingMessages
{
    public class MappingDeletedViewMessage
    {
        public int ViewId { get; set; }
        public int EndpointId { get; set; }
        public int TemplateId { get; set; }
    }
}
