﻿using _2.InfoScreen.API.Mapping.Application.Commands.MappedDataCommands;
using _2.InfoScreen.API.Mapping.Application.Commands.MappingCommands;
using _2.InfoScreen.API.Mapping.Application.Managers;
using _2.InfoScreen.API.Mapping.Application.Validators.Interface;
using _3.InfoScreen.API.Mapping.Infrastructure.Interfaces;
using _3.InfoScreen.API.Mapping.Infrastructure.Service.MappedData;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Models.Entities;
using _4.InfoScreen.API.Mapping.Domain.Result;
using AutoMapper;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _2.InfoScreen.API.Mapping.Application.Handlers.MappedDataHandlers
{
    public class GetMappedDataHandler : IRequestHandler<GetMappedDataCommand, Result<List<MappedDynamicModelDTO>>>
    {
        private IMapper _mapper;
        private IMappingValidateManager _validateManager;
        private IIntegrationService _mappedDataService;
        private IMappingRepository _repo;
        private MappedDataManager _mappedDataManager;

        public GetMappedDataHandler(
            IMapper mapper, 
            IMappingValidateManager validateManager, 
            IIntegrationService mappedDataService,
            IMappingRepository repo,
            MappedDataManager mappedDataManager)
        {
            _mapper = mapper;
            _validateManager = validateManager;
            _mappedDataService = mappedDataService;
            _repo = repo;
            _mappedDataManager = mappedDataManager;
        }

        public async Task<Result<List<MappedDynamicModelDTO>>> Handle(GetMappedDataCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidataGetMappedDataRequest(request.RequestModel);
                var dataFromIntegration = await _mappedDataService.GetDataFromIntegrationServiceAsync(request.RequestModel.EndpointId);
                dataFromIntegration.ThrowOnError();

                var mappings = await _repo.GetManyByEndpointAndTemplateID(request.RequestModel.EndpointId, request.RequestModel.TemplateId);

                var result = _mappedDataManager.GetMappedData(mappings, dataFromIntegration.Data);
                result.ThrowOnError();

                return Result<List<MappedDynamicModelDTO>>.Success(result.Data);
            }
            catch (Exception e)
            {
                return Result<List<MappedDynamicModelDTO>>.Error(e);
            }
        }
    }
}
