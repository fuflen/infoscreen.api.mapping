﻿using _3.InfoScreen.API.Mapping.Infrastructure.Interfaces;
using _4.InfoScreen.API.Mapping.Domain.Models.MessageDTO;
using _4.InfoScreen.API.Mapping.Domain.Models.RabbitMQModels.MessageDTO;
using _4.InfoScreen.API.Mapping.Domain.Models.RabbitMQModels.MessageDTO.OutgoingMessages;
using _4.InfoScreen.API.Mapping.Domain.Models.RabbitMQModels.MessageDTO.OutgoingMessages.DeleteFailedMessages;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace _3.InfoScreen.API.Mapping.Infrastructure.Service
{
    public class RabbitMQMappingService : IMessagingGateway
    {
        private static RabbitMQConnectionModel _connectionModel;

        private const string MappingDeletedEndpointRoutingKey = "mapping.deleted.endpoint";
        private const string MappingDeletedTemplateRoutingKey = "mapping.deleted.template";
        private const string MappingDeletedViewRoutingKey = "mapping.deleted.view";

        private const string MappingDeleteFailedEndpointRoutingKey = "mapping.delete.failed.endpoint";
        private const string MappingDeleteFailedTemplateRoutingKey = "mapping.delete.failed.template";
        private const string MappingDeleteFailedViewRoutingKey = "mapping.delete.failed.view";

        public RabbitMQMappingService(RabbitMQConnectionService connection)
        {
            _connectionModel = connection.GetConnectionModel();
        }

        public void SendMappingDeletedEndpointMessage(int endpointId)
        {
            var model = new MappingDeletedEndpointMessage()
            {
                EndpointId = endpointId
            };

            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));

            SendMessage(body, MappingDeletedEndpointRoutingKey);
        }

        public void SendMappingDeletedTemplateMessage(int templateId)
        {
            var model = new MappingDeletedTemplateMessage()
            {
                TemplateId = templateId
            };

            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));

            SendMessage(body, MappingDeletedTemplateRoutingKey);
        }

        public void SendMappingDeletedViewMessage(int endpointId, int templateId, int viewId)
        {
            var model = new MappingDeletedViewMessage()
            {
                EndpointId = endpointId,
                TemplateId = templateId,
                ViewId = viewId
            };

            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));

            SendMessage(body, MappingDeletedViewRoutingKey);
        }

        public void SendMappingDeleteFailedEndpointMessage(int endpointId)
        {
            var model = new MappingDeleteFailedEndpointMessage()
            {
                EndpointId = endpointId
            };

            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));

            SendMessage(body, MappingDeleteFailedEndpointRoutingKey);
        }

        public void SendMappingDeleteFailedTemplateMessage(int templateId)
        {
            var model = new MappingDeleteFailedTemplateMessage()
            {
                TemplateId = templateId
            };

            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));

            SendMessage(body, MappingDeleteFailedTemplateRoutingKey);
        }

        public void SendMappingDeleteFailedViewMessage(int endpointId, int templateId, int viewId)
        {
            var model = new MappingDeleteFailedViewMessage()
            {
                EndpointId = endpointId,
                TemplateId = templateId,
                ViewId = viewId
            };

            byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));

            SendMessage(body, MappingDeleteFailedViewRoutingKey);
        }


        private void SendMessage(byte[] message, string routingKey)
        {

            _connectionModel.Model.BasicPublish(_connectionModel.ExchangeName, routingKey, _connectionModel.Properties, message);
        }
    }
}
