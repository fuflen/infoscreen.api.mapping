﻿using _2.InfoScreen.API.Mapping.Application.Commands.MappingCommands;
using _2.InfoScreen.API.Mapping.Application.Validators.Interface;
using _3.InfoScreen.API.Mapping.Infrastructure.Interfaces;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Models.Entities;
using _4.InfoScreen.API.Mapping.Domain.Result;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _2.InfoScreen.API.Mapping.Application.Handlers.MappingHandlers
{
    public class CreateManyHandler : IRequestHandler<CreateManyCommand, Result<List<MappingDTO>>>
    {
        private IMappingRepository _repo;
        private IMapper _mapper;
        IMappingValidateManager _validateManager;

        public CreateManyHandler(IMappingRepository repo, IMapper mapper, IMappingValidateManager validateManager)
        {
            _repo = repo;
            _mapper = mapper;
            _validateManager = validateManager;
        }

        public async Task<Result<List<MappingDTO>>> Handle(CreateManyCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _validateManager.ValidateCreateManyRequest(request.RequestModel);

                List<MappingModel> model = _mapper.Map<List<MappingModel>>(request.RequestModel.DTOs);

                var entity = await _repo.CreateMany(model);
                List<MappingDTO> dtos = _mapper.Map<List<MappingDTO>>(entity);

                return Result<List<MappingDTO>>.Success(dtos);
            }
            catch (Exception e)
            {
                return Result<List<MappingDTO>>.Error(e);
            }
        }


    }
}
