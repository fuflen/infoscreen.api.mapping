﻿using _2.InfoScreen.API.Mapping.Application.Requests.CRUD;
using _2.InfoScreen.API.Mapping.Application.Requests.MappedData;
using _4.InfoScreen.API.Mapping.Domain.Models.DTO;
using _4.InfoScreen.API.Mapping.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2.InfoScreen.API.Mapping.Application.Validators.Interface
{
    public interface IMappingValidateManager
    {
        void ValidateCreateManyRequest(CreateManyRequest<MappingDTO> request);
        void ValidateDeleteManyByViewEndpointIdRequest(DeleteManyByEndpointAndTemplateIdRequest request);
        void ValidateUpdateManyRequest(UpdateManyRequest<MappingDTO> request);
        void ValidateGetManyByViewEndpointIdRequest(GetManyByEndpointAndTemplateIdRequest request);
        void ValidataGetMappedDataRequest(GetMappedDataRequest request);

    }
}
